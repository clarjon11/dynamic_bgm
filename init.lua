dbgm = {} 
dbgm.path = minetest.get_modpath("dynamic_bgm")
dbgm.ephemeral = true
dbgm.interval = 240 -- should be longer than the longest sound file, the interval to try to play music
dbgm.debug = false
dbgm.player_settings = {}
dbgm.info_color = "#f4b41b"
dbgm.error_color = "#e6482e"



local storage = minetest.get_mod_storage()

function dbgm.get_player_settings_from_storage(p_name)
        return minetest.deserialize(storage:get_string("settings_"..p_name))
end

function dbgm.save_player_settings_to_storage(p_name)
    storage:set_string("settings_"..p_name, minetest.serialize(dbgm.player_settings[p_name]))
end




dofile(dbgm.path.."/api.lua")



minetest.register_on_joinplayer(function(player)

        local p_name = player:get_player_name()
        dbgm.player_settings[p_name] = dbgm.get_player_settings_from_storage(p_name) or {music_on = false}
        dbgm.save_player_settings_to_storage(p_name)
        if not dbgm.player_settings[p_name].music_on then
                minetest.chat_send_player(p_name,minetest.colorize(dbgm.info_color, "[dbgm] [ i ] Your background music is disabled. /bgm to enable it (Not recommended for slow connections.)"))
        end
        
end)

minetest.register_chatcommand("bgm", {
        privs = {
        },
        func = function(p_name, param)
                if not minetest.get_player_by_name(p_name) then
                        return false, minetest.colorize(dbgm.error_color,"[ ! ] You must be online to run this command")
                end
                dbgm.player_settings[p_name].music_on = not(dbgm.player_settings[p_name].music_on)
                dbgm.save_player_settings_to_storage(p_name)
                if dbgm.player_settings[p_name].music_on then
                        return true, minetest.colorize(dbgm.info_color,"[dbgm] [ i ] Your background music has been enabled.")
                else
                        dbgm.fade_music(p_name)
                        return true, minetest.colorize(dbgm.info_color,"[dbgm] [ i ] Your background music has been disabled.")
                end
        end,
    })
